/*
 * File:   main.c
 * Author: Marcus Alexander Tjomsaas
 *
 * Created on 04 March 2020, 15:26
 */


#include "allcode_api.h"

int sampleTime = 30; // 30 ms
//int target = 27; //30 ticks per 30ms

int KP = 2;
int KI = 20;
int KD = 3;

double ILeft = 0;
double IRight = 0;

int encoderLeft = 0;
int encoderRight = 0;
double speedLeft = 0;
double speedRight = 0;

int errorLeft_prev = 0;
int errorRight_prev = 0;

unsigned long prevTime = 0;

double distPID = 0;
int encoderTotal = 0;
//------------------------------------------
int hasTurned_state = 0;
//------------------------------------------
bool frontWall_detected = false;
bool leftWall_detected = false;
bool rightWall_detected = false;
//------------------------------------------
int maze [5][5];
int robotPosX = 3; //0-4, 5 cells
int robotPosY = 2; //0-5, 5 cells
int robotDir = 0; //0 north, 1 east, 2 south, 3 west

//------------------------------MAZE INIT------------------------

void mazeInit() {
    int i = 0;
    int j = 0;

    for (i = 0; i < 5; i++) {
        for (j = 0; j < 5; j++) {
            maze[i][j] = 0;
        }
    }
    FA_LCDLine(0, 0, 30, 0);
    FA_LCDLine(0, 0, 0, 30);
    FA_LCDLine(0, 30, 30, 30);
    FA_LCDLine(30, 0, 30, 30);
}

/*
 * ---------------------------LCD----------------------------------
 */


void drawWall(int x1, int y1, int x2, int y2) {
    FA_LCDLine(x1, y1, x2, y2);
}

void updateMaze() {
    int x1 = robotPosX * 6; //pos * length of wall in px
    int y1 = robotPosY * 6;
    int x2 = x1 + 6; // from start pos + length of wall
    int y2 = y1 + 6;
    //draw a dot in the middle of the cell to display visited cell
    drawWall(x1 + 3, y1 + 3, x1 + 3, y1 + 3);
    //0 north, 1 east, 2 south, 3 west
    switch (robotDir) {
        case 0:
            if (frontWall_detected) {
                drawWall(x1, y1, x2, y1);
            }
            if (leftWall_detected) {
                drawWall(x1, y1, x1, y2);
            }
            if (rightWall_detected) {
                drawWall(x2, y1, x2, y2);
            }
            break;
        case 1:
            if (frontWall_detected) {
                drawWall(x2, y1, x2, y2);
            }
            if (leftWall_detected) {
                drawWall(x1, y1, x2, y1);
            }
            if (rightWall_detected) {
                drawWall(x1, y2, x2, y2);
            }
            break;

        case 2:
            if (frontWall_detected) {
                drawWall(x1, y2, x2, y2);
            }
            if (leftWall_detected) {
                drawWall(x2, y1, x2, y2);

            }
            if (rightWall_detected) {
                drawWall(x1, y1, x1, y2);
            }
            break;
        case 3:
            if (frontWall_detected) {
                drawWall(x1, y1, x1, y2);
            }
            if (leftWall_detected) {
                drawWall(x1, y2, x2, y2);
            }
            if (rightWall_detected) {
                drawWall(x1, y1, x2, y1);
            }
            break;
    }
    maze[robotPosX][robotPosY] = 1;
}

//---------------------PID CONTROLLER (DRIVE STRAIGHT)---------------

double PIDController(int channel, int error, int errorPrev) {
    double PID = 0;
    double P = error / KP;
    double I = (error * sampleTime) / KI;
    double D = (((error - errorPrev) / sampleTime)) / KD;

    switch (channel) {
            //-------------Left wheel----------------------
        case 0:
            ILeft += I;
            PID = P + ILeft + D;
            break;
            //-------------Right wheel----------------------
        case 1:
            IRight += I;
            PID = P + IRight + D;
            break;
    }
    return PID;
}

//We know the limits of the motor, therefore the robot should not be able to exceed them.
//Also since the encoder is only counting positive in either direction.
//Therefore we have to eliminate the possibility when the speed is negative and target positive and speed positive and target negative.

double constrainSpeed(int speed, int target) {
    if (speed > 100) speed = 100;
    if (speed < -100) speed = -100;
    if (target <= 0 && speed >= 0) speed = 0;
    if (target >= 0 && speed <= 0) speed = 0;

    return speed;
}


//Checks whether or not the robot is achiving the target values (encoderticks per sampletime). If not, adjust and make it achieve the target ticks.

void speedPID(int targetLeft, int targetRight) {
    int encoderLeft = FA_ReadEncoder(CHANNEL_LEFT);
    int encoderRight = FA_ReadEncoder(CHANNEL_RIGHT);

    //If the motors are going backwards, the encoder ticks needs to go backwards
    if (targetLeft < 0) encoderLeft *= -1;
    if (targetRight < 0) encoderRight *= -1;
    encoderTotal += (encoderLeft + encoderRight) / 2;

    int errorLeft = targetLeft - encoderLeft;
    int errorRight = targetRight - encoderRight;

    double speedL = PIDController(0, errorLeft, errorLeft_prev);
    double speedR = PIDController(1, errorRight, errorRight_prev);

    speedL = constrainSpeed(speedL, targetLeft);
    speedR = constrainSpeed(speedR, targetRight);

    FA_SetMotors(speedL, speedR);

    errorLeft_prev = errorLeft;
    errorRight_prev = errorRight;
}

//This is the speed method for the robot wheels. 

void setSpeed(int targetLeft, int targetRight) {
    if (prevTime + sampleTime <= FA_ClockMS()) {
        speedPID(targetLeft, targetRight);
        FA_ResetEncoders();
        prevTime = FA_ClockMS();
    }
}

//This needs to be reset every time a different target value is used for the PID controller. 

void resetPIDController() {
    errorLeft_prev = 0;
    errorRight_prev = 0;

    ILeft = 0;
    IRight = 0;

    FA_ResetEncoders();
}
//--------------------------SENSORS------------------------
//Reading average of a IR sensor. This is to eliminate noise.

double readIRSensor(int sensor) {
    double IRAverage = 0;
    int currentSample = 0;
    int totalSample = 50;
    while (currentSample < totalSample) {
        int IRValue = FA_ReadIR(sensor); //0 left, 1 left-front, 2 front, 3 right-front .....

        IRAverage += (double) IRValue;
        currentSample++;
    }
    IRAverage /= currentSample;

    return IRAverage;
}
//Low values == low light (around 200)
//High values == alot of light (from 1000 and above)

int readLightSensor() {
    return FA_ReadLight();
}

// Low values == black (close to 0)
//High values == white (from 300 and above)

int readLineSensor() {
    return FA_ReadLine(0);
}
//--------------------------BEHAVIOURS------------------------
//Maze is initialized with 0's, if the robot has explored the whole maze there should be no 0's and return true, else false.

bool exploredMaze() {
    bool explored = true;
    int i = 0;
    int j = 0;

    for (i = 0; i < 5; i++) {
        for (j = 0; j < 5; j++) {
            if (maze[i][j] == 0) {
                explored = false;
                break;
            }
        }
    }

    return explored;
}

//Update the robot position.

void updatePos() {
    switch (robotDir) {
        case 0:
            robotPosY++;
            break;
        case 1:
            robotPosX++;
            break;
        case 2:
            robotPosY--;
            break;
        case 3:
            robotPosX--;
            break;
    }
}

void turnLeft(int angle) {
    angle += (angle / 45);
    FA_Left(angle); //93 works for 90

}

void turnRight(int angle) {
    angle -= (angle / 90);
    FA_Right(angle); //89 works for 90
}

//If line detected, turn LCD brightness to 0

bool lineDetected(bool lineDetected) {
    if (readLineSensor() < 300) {
        lineDetected = true;
    }
    return lineDetected;
}

//If nest found, play a note and map nest to the maze.

void darkDetection() {
    if (readLightSensor() < 200) {
        FA_PlayNote(5000, 10);
        FA_LCDBacklight(100);
        maze[robotPosX] [robotPosY] = 2;
    } else {
        FA_LCDBacklight(50);
    }
}



//Detects if a sensor has detected a wall or not

void wallDetect(int sensor) {

    int IR = readIRSensor(sensor);
    if (IR > 20) {
        switch (sensor) {
            case 0:
                leftWall_detected = true;
                break;
            case 2:
                frontWall_detected = true;
                break;
            case 4:
                rightWall_detected = true;
                break;
        }
    } else {
        switch (sensor) {
            case 0:
                leftWall_detected = false;
                break;
            case 2:
                frontWall_detected = false;
                break;
            case 4:
                rightWall_detected = false;
                break;
        }
    }
}
//Check for walls around the robot

void wallsDetect() {
    wallDetect(0);
    wallDetect(2);
    wallDetect(4);
}

//Make a turn depending on what walls have been detected.
//Allways try one of these in this order: turn left, forward, turn right, u-turn
//Update robot direction when executing a turn

void turn() {
    if (!leftWall_detected) {
        turnLeft(90);
        robotDir = (robotDir - 1 + 4) % 4;
        wallsDetect();
    } else if (leftWall_detected && !frontWall_detected) {
        //forward
    } else if (leftWall_detected && frontWall_detected && !rightWall_detected) {
        turnRight(90);
        robotDir = (robotDir + 1 + 4) % 4;
        wallsDetect();
    } else if (leftWall_detected && frontWall_detected && rightWall_detected) {
        turnLeft(180);
        robotDir = (robotDir + 2 + 4) % 4;
        wallsDetect();
    }
}

//Turn away from the wall if to close and then check if youre still too close. Repeat.

void wallAdjust() {
    static bool hasTurned = false;
    if (readIRSensor(2) > 170) { //Front
        frontWall_detected = true;
        hasTurned_state = 1;

    } else if (readIRSensor(1) > 170) { // left-front
        setSpeed(30, -30);
        hasTurned = true;
        wallAdjust();

    } else if (readIRSensor(3) > 170) { //right-front
        setSpeed(-30, 30);
        hasTurned = true;
        wallAdjust();
    }

    //Have to reset the PID controller since we have updated the target values (speed).
    if (hasTurned) {
        resetPIDController();
        hasTurned = false;
    }
}

//Drive forward until a line is detected. If a front wall is detected, stop looking. 

void lookForLine() {
    bool lineDetect = false;
    while (!lineDetect && !frontWall_detected) {
        setSpeed(30, 30);
        lineDetect = lineDetected(lineDetect);
        wallAdjust();
        darkDetection();
    }
}

//Distance in mm, 90 mm is the distance needed to make the robot go to the middle
//because there is a 20-ish mm difference from the center to the wheels and to make them go to the center we add that to the radius of the maze

//Go forward until you have reached the middle of the next cell or found a front wall.

void forwardToNextCell(int distance) {
    //0.32 mm per encoder value
    double encoderDesired = distance / 0.32;
    lookForLine();

    switch (hasTurned_state) {
        case 0:
            //Since a line has been found, the robot is now in a new cell, update robot position.
            updatePos();
            encoderTotal = 0;
            while (encoderDesired >= encoderTotal && !frontWall_detected) {
                setSpeed(30, 30);
                wallAdjust();
                darkDetection();
            }

            break;
        case 1:
            //If a front wall has been found, repeat (or so was the intention).
            encoderTotal = 0;
            hasTurned_state = 0;
//            forwardToNextCell(90); works because of luck, not needed
            break;
    }
}

int main(void) {
    FA_RobotInit(); // MUST call this before any other robot functions
    FA_LCDBacklight(50); // Switch on backlight (half brightness)
    FA_LCDPrint("Hello", 5, 20, 25, FONT_NORMAL, LCD_OPAQUE); // Say hi!
    FA_DelayMillis(1000); // Pause 1 sec
    FA_LCDClear();
    mazeInit();
    prevTime = FA_ClockMS();

    //--------------------------MAIN LOOP------------------------
    while (1) {
        
        while (!exploredMaze()) {
            wallsDetect();
            updateMaze();
            turn();
            resetPIDController();
            forwardToNextCell(90);
        }

        while (exploredMaze()) {
            setSpeed(-30, 30);
        }
    }

    return 0;
}